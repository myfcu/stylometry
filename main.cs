using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.pipeline;
using edu.stanford.nlp.sentiment;
using edu.stanford.nlp.util;
using java.util;
 
namespace NLPFunctions
{
    class NLPFunctions
    {
        private string text;
        private List<string> sentences;
        private List<string> words;
 
        public NLPFunctions(string text)
        {
            this.text = text;
            sentences = GetSentences(text);
            words = TokenizeWords(text);
        }
 
        public double AverageIdiomsPerSample()
        {
            var idioms = new List<string>() { "A bird in the hand is worth two in the bush", "A bitter pill to swallow", "A blessing in disguise", "A diamond in the rough", "A dime a dozen", "A drop in the bucket", "A fool and his money are soon parted", "A leopard can't change its spots", "A leopard doesn't change its spots", "A penny for your thoughts", "A penny saved is a penny earned", "A picture is worth a thousand words", "A picture paints a thousand words", "A piece of cake", "A rose by any other name would smell as sweet", "A stitch in time saves nine", "A taste of your own medicine", "A watched pot never boils", "Actions speak louder than words", "Add fuel to the fire", "Add insult to injury", "All bark and no bite", "All ears", "All in the same boat", "All that glitters is not gold", "All thumbs", "An arm and a leg", "An axe to grind", "An elephant never forgets", "Apple of my eye", "As cool as a cucumber", "As easy as pie", "As fit as a fiddle", "As mad as a hatter", "As old as the hills", "As the crow flies", "At the drop of a hat", "Back to the drawing board", "Barking up the wrong tree", "Be careful what you wish for", "Beat around the bush", "Behind the eight ball", "Better late than never", "Better safe than sorry", "Between a rock and a hard place", "Bite off more than you can chew", "Bite the bullet", "Bite the hand that feeds you", "Blessing in disguise", "Blood is thicker than water", "Break a leg", "Break the bank", "Break the ice", "Bull in a china shop", "Burn the midnight oil", "Burning the midnight oil", "Bury the hatchet", "Butterflies in your stomach", "By hook or by crook", "By the skin of your teeth", "Call it a day", "Can of worms", "Can't hold a candle to", "Can't judge a book by its cover", "Cast pearls before swine", "Cat got your tongue?", "Caught between a rock and a hard place", "Caught red-handed", "Champ at the bit", "Chase your tail", "Chew someone out", "Chew the fat", "Chin up", "Chip on your shoulder", "Clean as a whistle", "Clean slate", "Clear the air", "Close but no cigar", "Close call", "Cold turkey", "Come hell or high water", "Comparing apples to oranges", "Cost a pretty penny", "Cost an arm and a leg", "Costs an arm and a leg", "Couch potato", "Crocodile tears", "Cry over spilled milk", "Cry wolf", "Curiosity killed the cat", "Cut corners", "Cut the mustard", "Cut to the chase", "Cutting corners", "Dark horse", "Dead as a doornail", "Devil's advocate", "Diamond dozen", "Different kettle of fish", "Do or die", "Don't beat around the bush", "Don't bite the hand that feeds you", "Don't count your chickens before they hatch", "Don't cry over spilled milk", "Don't cry over spilt milk", "Don't judge a man until you've walked a mile in his shoes", "Don't put all your eggs in one basket", "Don't put the cart before the horse", "Don't throw the baby out with the bathwater", "Down the drain", "Down to the wire", "Draw the line", "Drive someone crazy", "Drive someone up the wall", "Dropping like flies", "Dumb as a doornail", "Dutch courage", "Easy come,  easy go", "Every cloud has a silver lining", "Every dog has its day", "Every man for himself", "Everything but the kitchen sink", "Eye for an eye", "Faint heart never won fair lady", "Fall from grace", "Famous last words", "Far cry", "Feather in one's cap", "Feeling blue", "Fight tooth and nail", "Fighting tooth and nail", "Finger lickin' good", "Fish or cut bait", "Fish out of water", "Fit as a fiddle", "Fit like a glove", "Flat as a pancake", "Flip your lid", "Flog a dead horse", "Fly off the handle", "Flying high", "Foaming at the mouth", "For the birds", "From rags to riches", "From the horse's mouth", "Get a grip", "Get a second wind", "Get cold feet", "Get down to brass tacks", "Get off your high horse", "Get the ball rolling", "Get the show on the road", "Get to the point", "Give someone the benefit of the doubt", "Give someone the cold shoulder", "Give the benefit of the doubt", "Go against the grain", "Go all out", "Go down in flames", "Go down in history", "Go for broke", "Go for the gold", "Go off the deep end", "Go the distance", "Go the extra mile", "Go with the flow", "Good things come to those who wait", "Grass is always greener on the other side", "Green with envy", "Grin and bear it", "Hair of the dog", "Half-baked", "Hang in there", "Haste makes waste", "Have a heart", "Have a lot on your plate", "Have one's cake and eat it too", "Have your cake and eat it too", "He has his head in the clouds", "He who laughs last,  laughs best", "He's a chip off the old block", "He's a wolf in sheep's clothing", "He's all thumbs", "He's barking up the wrong tree", "He's got a screw loose", "He's got his work cut out for him", "He's like a fish out of water", "He's not playing with a full deck", "He's off his rocker", "He's on cloud nine", "He's sitting pretty", "He's the apple of my eye", "Head in the clouds", "Head over heels", "Hear a pin drop", "Hit the books", "Hit the nail on the head", "Hitch your wagon to a star", "Hold your horses", "Home is where the heart is", "Honesty is the best policy", "Hungry as a bear", "Icing on the cake", "In a bind", "In a nutshell", "In a pickle", "In a pinch", "In hot water", "In the bag", "In the black", "In the blink of an eye", "In the doghouse", "In the driver's seat", "In the red", "In the same boat", "In the thick of it", "It takes two to tango", "It's a piece of pie", "It's a small world", "It's all Greek to me", "It's as easy as ABC", "It's not rocket science", "It's raining cats and dogs", "It's the thought that counts", "Jump on the bandwagon", "Jump ship", "Jump the gun", "Keep a stiff upper lip", "Keep an eye out", "Keep your chin up", "Keep your fingers crossed", "Keep your nose to the grindstone", "Kick the bucket", "Kick the habit", "Kill time", "Kill two birds with one stone", "Knock on wood", "Know the ropes", "Last but not least", "Last straw", "Laugh all the way to the bank", "Leave no stone unturned", "Let sleeping dogs lie", "Let the cat out of the bag", "Let the chips fall where they may", "Life is a bed of roses", "Life is a box of chocolates", "Life is like a rollercoaster", "Light at the end of the tunnel", "Like a bat out of hell", "Like a fish in water", "Like a fish out of water", "Like a moth to a flame", "Like a ton of bricks", "Like pulling teeth", "Like riding a bike", "Like shooting fish in a barrel", "Like two peas in a pod", "Live and learn", "Live and let live", "Living in the lap of luxury", "Long in the tooth", "Loose cannon", "Love is blind", "Make a long story short", "Make ends meet", "Make hay while the sun shines", "Make my day", "Make someone's day", "Make the grade", "Make waves", "March to the beat of your own drum", "Means to an end", "Miss the boat", "More than meets the eye", "Mumbo jumbo", "Naked as a jaybird", "Neck of the woods", "Needle in a haystack", "Nervous as a long-tailed cat in a room full of rocking chairs", "Nest egg", "Never say never", "Nip it in the bud", "No guts,   no glory", "No pain,   no gain", "No pain,  no gain", "No stone left unturned", "No use crying over spilt milk", "Not a chance in hell", "Not a happy camper", "Not my cup of tea", "Not playing with a full deck", "Off the beaten path", "Off the cuff", "Off the hook", "Off the wall", "On a roll", "On cloud nine", "On pins and needles", "On the ball", "On the fence", "On the same page", "On thin ice", "Once bitten,  twice shy", "Once in a blue moon", "One bad apple spoils the whole bunch", "One man's trash is another man's treasure", "Out of left field", "Out of the frying pan and into the fire", "Out of the woods", "Out on a limb", "Over the moon", "Over the top", "Paddle your own canoe", "Paint the town red", "Penny for your thoughts", "Penny wise,  pound foolish", "Picture perfect", "Pie in the sky", "Piece of cake", "Piece of the action", "Pig in a poke", "Pipe down", "Pipe dream", "Play it by ear", "Pot calling the kettle black", "Pull out all the stops", "Pull someone's leg", "Pull the wool over someone's eyes", "Put a sock in it", "Put all your eggs in one basket", "Put on a brave face", "Put the cart before the horse", "Put your foot in your mouth", "Rain on someone's parade", "Raining cats and dogs", "Read between the lines", "Red herring", "Ride shotgun", "Right as rain", "Rise and shine", "Roll out the red carpet", "Rome wasn't built in a day", "Rough around the edges", "Rub salt in the wound", "Rub someone the wrong way", "Ruffle some feathers", "Ruffle someone's feathers", "Rule of thumb", "Run amok", "Run circles around", "Run for your life", "Run of the mill", "Run out of steam", "Saddle up", "Salt of the earth", "Saved by the bell", "Scapegoat", "Scot-free", "See eye to eye", "Sell like hotcakes", "Set in stone", "Shake a leg", "Shape up or ship out", "Shoot for the moon", "Shoot from the hip", "Shoot the breeze", "Shoot the moon", "Short end of the stick", "Show your true colors", "Sick as a dog", "Sight for sore eyes", "Silence is golden", "Silver lining", "Sitting duck", "Sitting on the fence", "Six feet under", "Six of one,  half dozen of the other", "Skating on thin ice", "Sleep on it", "Slippery slope", "Slow and steady wins the race", "Smell a rat", "Smooth sailing", "Snug as a bug in a rug", "So far so good", "Something smells fishy", "Sour grapes", "Speak of the devil", "Spill the beans", "Spill your guts", "Spinning your wheels", "Spitting image", "Splitting hairs", "Spread like wildfire", "Spring to life", "Square peg in a round hole", "Stab in the dark", "Stab someone in the back", "Stand your ground", "Start from scratch", "Steal someone's thunder", "Stick to your guns", "Sticks and stones", "Stitch in time saves nine", "Straight and narrow", "Straight from the horse's mouth", "Stuck in a rut", "Sweating bullets", "Take a hike", "Take a load off", "Take a rain check", "Take a stab at", "Take it on the chin", "Take it with a grain of salt", "Take the bull by the horns", "Take the cake", "Take the plunge", "Take with a grain of salt", "Talk of the town", "Talk the talk,  walk the walk", "Taste of your own medicine", "The apple doesn't fall far from the tree", "The ball is in your court", "The ball's in your court", "The best of both worlds", "The bigger they are,  the harder they fall", "The devil is in the details", "The devil you know is better than the devil you don't", "The early bird catches the worm", "The early bird gets the worm", "The elephant in the room", "The grass is always greener", "The handwriting is on the wall", "The honeymoon is over", "The icing on the cake", "The in-crowd", "The last straw", "The more the merrier", "The proof is in the pudding", "The sky's the limit", "The squeaky wheel gets the grease", "The whole ball of wax", "The whole nine yards", "There are plenty of fish in the sea", "There's a method to my madness", "There's no place like home", "There's no such thing as a free lunch", "There's no time like the present", "Think outside the box", "Third wheel", "Through thick and thin", "Throw caution to the wind", "Throw in the towel", "Throw someone under the bus", "Tie the knot", "Til the cows come home", "Time flies when you're having fun", "Time flies", "Time heals all wounds", "Tip of the iceberg", "To each his own", "Toe the line", "Tongue in cheek", "Top dog", "Turn a blind eye", "Turn over a new leaf", "Twist someone's arm", "Two heads are better than one", "Under the gun", "Under the table", "Under the weather", "Up a creek without a paddle", "Up for grabs", "Up in arms", "Up in smoke", "Up in the air", "Up to par", "Walk on eggshells", "Waste not,  want not", "Watch your back", "We'll cross that bridge when we come to it", "Wet behind the ears", "When in Rome,   do as the Romans do", "When in doubt,  do without", "When pigs fly", "When the going gets tough,  the tough get going", "Where there's smoke,  there's fire", "Whole shebang", "Wild goose chase", "You can lead a horse to water,   but you can't make it drink", "You can't have your cake and eat it too", "You can't judge a book by its cover", "You can't make an omelette without breaking eggs", "You can't teach an old dog new tricks", "You reap what you sow", "Your guess is as good as mine"ur guess is as good as mine"e ears", "When in Rome,   do as the Romans do", "When in doubt,  do without", "When pigs fly", "When the going gets tough,  the tough get going", "Where there's smoke,  there's fire", "Whole shebang", "Wild goose chase", "You can lead a horse to water,   but you can't make it drink", "You can't have your cake and eat it too", "You can't judge a book by its cover", "You can't make an omelette without breaking eggs", "You can't teach an old dog new tricks", "You reap what you sow", "Your guess is as good as mine"ur guess is as good as mine"me,   do as the Romans do", "When in doubt,  do without", "When pigs fly", "When the going gets tough,  the tough get going", "Where there's smoke,  there's fire", "Whole shebang", "Wild goose chase", "You can lead a horse to water,   but you can't make it drink", "You can't have your cake and eat it too", "You can't judge a book by its cover", "You can't make an omelette without breaking eggs", "You can't teach an old dog new tricks", "You reap what you sow", "Your guess is as good as mine"ur guess is as good as mine"e ears", "When in Rome,   do as the Romans do", "When in doubt,  do without", "When pigs fly", "When the going gets tough,  the tough get going", "Where there's smoke,  there's fire", "Whole shebang", "Wild goose chase", "You can lead a horse to water,   but you can't make it drink", "You can't have your cake and eat it too", "You can't judge a book by its cover", "You can't make an omelette without breaking eggs", "You can't teach an old dog new tricks", "You reap what you sow", "Your guess is as good as mine"ur guess is as good as mine" };
 
            int idiomCount = sentences.Count(sentence => idioms.Any(idiom => sentence.ToLower().Contains(idiom)));
            return (double)idiomCount / sentences.Count;
        }
 
        public double AverageAcronymsPerSample()
        {
            int acronymCount = words.Count(word => word.Length > 1 && word.All(char.IsUpper) && !IsCommonWord(word));
            return (double)acronymCount / sentences.Count;
        }
 
        private bool IsCommonWord(string word)
        {
            HashSet<string> dict = LoadDictionary();
            return dict.Contains(word.ToLower());
        }
 
        private HashSet<string> LoadDictionary()
        {
            string filename = "dictionary.txt";
            string[] lines = File.ReadAllLines(filename);
            HashSet<string> dict = new HashSet<string>();
            foreach (string line in lines)
            {
                dict.Add(line.ToLower());
            }
            return dict;
        }
 
        public double AverageAbbreviationsPerSample()
        {
            int abbreviationCount = words.Count(word => word.EndsWith(".") && !IsSentenceEndPunctuation(word));
            return (double)abbreviationCount / sentences.Count;
        }
 
        private bool IsSentenceEndPunctuation(string word)
        {
            return word == "." || word == "?" || word == "!";
        }
 
        public double AverageNamedEntitiesPerSample()
        {
            var namedEntities = StanfordNamedEntityRecognition(text);
 
            int namedEntityCount = namedEntities.Count;
            return (double)namedEntityCount / sentences.Count;
        }
 
        private static List<string> StanfordNamedEntityRecognition(string text)
        {
            var props = new java.util.Properties();
            props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner");
            var pipeline = new StanfordCoreNLP(props);
 
            var document = new Annotation(text);
            pipeline.annotate(document);
            var sentences = document.get(typeof(CoreAnnotations.SentencesAnnotation)) as ArrayList;
 
            var namedEntities = new List<string>();
            foreach (CoreMap sentence in sentences)
            {
                var tokens = sentence.get(typeof(CoreAnnotations.TokensAnnotation)) as ArrayList;
                foreach (CoreLabel token in tokens)
                {
                    string ner = token.get(typeof(CoreAnnotations.NamedEntityTagAnnotation)).ToString();
                    if (ner != "O")
                    {
                        namedEntities.Add(token.word());
                    }
                }
            }
 
            return namedEntities;
        }
 
        public double AverageAdjectivesPerSample()
        {
            var posTags = StanfordPOSTagger(text);
 
            int adjectiveCount = posTags.Count(tag => tag == "JJ");
            return (double)adjectiveCount / sentences.Count;
        }
private static List<string> StanfordPOSTagger(string text)
        {
            var props = new java.util.Properties();
            props.setProperty("annotators", "tokenize, ssplit, pos");
            var pipeline = new StanfordCoreNLP(props);
 
            var document = new Annotation(text);
            pipeline.annotate(document);
            var sentences = document.get(typeof(CoreAnnotations.SentencesAnnotation)) as ArrayList;
 
            var posTags = new List<string>();
            foreach (CoreMap sentence in sentences)
            {
                var tokens = sentence.get(typeof(CoreAnnotations.TokensAnnotation)) as ArrayList;
                foreach (CoreLabel token in tokens)
                {
                    string pos = token.get(typeof(CoreAnnotations.PartOfSpeechAnnotation)).ToString();
                    posTags.Add(pos);
                }
            }
             return posTags;
        }
 
        public string DetermineSentiment()
        {
            var sentimentScores = StanfordSentimentAnalysis(text);
 
            double averageSentiment = sentimentScores.Average();
            if (averageSentiment <= -0.5)
            {
               return "Very negative";
            }
            else if (averageSentiment > -0.5 && averageSentiment < 0)
            {
                return "Somewhat negative";
            }
            else if (averageSentiment == 0)
            {
                return "Neutral";
            }
            else if (averageSentiment > 0 && averageSentiment < 0.5)
            {
                return "Somewhat positive";
            }
            else
            {
                return "Very positive";
            }
        }
 
        private static List<double> StanfordSentimentAnalysis(string text)
        {
            var props = new java.util.Properties();
            props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
            var pipeline = new StanfordCoreNLP(props);
 
            var document = new Annotation(text);
            pipeline.annotate(document);
            var sentences = document.get(typeof(CoreAnnotations.SentencesAnnotation)) as ArrayList;
 
            var sentimentScores = new List<double>();
            foreach (CoreMap sentence in sentences)
            {
                var tree = sentence.get(typeof(SentimentCoreAnnotations.AnnotatedTree)) as Tree;
                var sentiment = RNNCoreAnnotations.getPredictedClass(tree);
                sentimentScores.Add((double)sentiment);
            }
 
            return sentimentScores;
        }
 
        public List<string> GetSentences(string text)
        {
            var props = new java.util.Properties();
            props.setProperty("annotators", "tokenize, ssplit");
            var pipeline = new StanfordCoreNLP(props);
 
            var document = new Annotation(text);
            pipeline.annotate(document);
            var sentences = document.get(typeof(CoreAnnotations.SentencesAnnotation)) as ArrayList;
 
            var sentenceList = new List<string>();
            foreach (CoreMap sentence in sentences)
            {
                sentenceList.Add(sentence.ToString());
            }
 
            return sentenceList;
        }
 
        public List<string> TokenizeWords(string text)
        {
            var props = new java.util.Properties();
            props.setProperty("annotators", "tokenize");
            var pipeline = new StanfordCoreNLP(props);
 
            var document = new Annotation(text);
            pipeline.annotate(document);
            var sentences = document.get(typeof(CoreAnnotations.SentencesAnnotation)) as ArrayList;
 
            var wordList = new List<string>();
            foreach (CoreMap sentence in sentences)
            {
                var tokens = sentence.get(typeof(CoreAnnotations.TokensAnnotation)) as ArrayList;
                foreach (CoreLabel token in tokens)
                {
                    wordList.Add(token.word());
                }
            }
 
            return wordList;
        }
    }
}
class TextAnalyzer
{
    private readonly string text;
    private readonly List<string> sentences;
    private readonly List<string> words;
 
    public TextAnalyzer(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            throw new ArgumentException("Text cannot be null or empty.");
        }
 
        this.text = text;
        this.sentences = text.Split(new[] { '.', '?', '!' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(sentence => sentence.Trim())
            .ToList();
 
        this.words = text.Split(new[] { ' ', '\t', '\n' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(word => word.Trim(new[] { '.', ',', ';', ':', '(', ')', '[', ']', '{', '}', '<', '>', '\'', '\"', '`', '“', '”', '‘', '’', '—', '-', '–', '—', '/', '\\', '|', '_', '*', '&', '^', '%', '$', '#', '@', '!', '?', '+', '=', '~', '§', '©', '®', '™', '·', '…', '°', '℃', '℉', '€', '£', '¥', '₹', '¢', '₽', '₱', '₪', '฿', '₫', '₩', '﷼' }))
            .ToList();
    }
public double AverageNumberOfWordsPerSentence()
    {
        int wordCount = words.Count;
        int sentenceCount = sentences.Count;
        return (double)wordCount / sentenceCount;
    }
 
    public double AverageSyllablesPerWord()
    {
        int syllableCount = words.Sum(word => CountSyllables(word));
        int wordCount = words.Count;
        return (double)syllableCount / wordCount;
    }
 
    public double AverageLettersPerWord()
    {
        int letterCount = words.Sum(word => CountLetters(word));
        int wordCount = words.Count;
        return (double)letterCount / wordCount;
    }
public double AverageSentencesPerParagraph()
    {
        int paragraphCount = text.Split(new[] { "\r\n\r\n", "\n\n" }, StringSplitOptions.RemoveEmptyEntries).Count();
        int sentenceCount = sentences.Count;
        return (double)sentenceCount / paragraphCount;
    }
 
    public double UseOfRepetitivePhrases()
    {
        int n = 3; // Use trigrams (3-word sequences) for example
        var ngrams = GetNGrams(n);
 
        var ngramCounts = new Dictionary<string, int>();
        foreach (var ngram in ngrams)
        {
            if (ngramCounts.ContainsKey(ngram))
            {
                ngramCounts[ngram]++;
            }
            else
            {
                ngramCounts.Add(ngram, 1);
            }
        }
 
        int repetitivePhraseCount = ngramCounts.Values.Count(count => count > 1);
        return (double)repetitivePhraseCount / sentences.Count;
    }
public double UseOfMisnomers()
    {
        var misnomerPairs = new Dictionary<string, string>()
        {
            { "hot ice", "water" },
            { "sunrise at sunset", "horizon" }
            // Add more misnomer pairs as needed
        };
 
        int misnomerCount = sentences.Count(sentence =>
            misnomerPairs.Any(pair =>
                sentence.ToLower().Contains(pair.Key) && !sentence.ToLower().Contains(pair.Value)));
 
        return (double)misnomerCount / sentences.Count;
    }
 
    public double CommonalityOfWordGroupings(int groupingSize)
    {
        var ngrams = GetNGrams(groupingSize);
        int commonGroupingCount = ngrams.Sum(ngram => CountNgramOccurrences(ngram));
        return (double)commonGroupingCount / sentences.Count;
    }
public double FrequencyOfAlternativeSpelling()
    {
        var alternativeSpellings = new Dictionary<string, string>()
        {
            { "color", "colour" },
            { "analyze", "analyse" }
            // Add more alternative spellings as needed
        };
 
        int alternativeSpellingCount = words.Count(word =>
            alternativeSpellings.Any(pair =>
                string.Equals(word, pair.Key, StringComparison.OrdinalIgnoreCase) ||
                string.Equals(word, pair.Value, StringComparison.OrdinalIgnoreCase)));
 
        return (double)alternativeSpellingCount / sentences.Count;
    }
 
    private List<string> GetNGrams(int n)
    {
        var ngrams = new List<string>();
        for (int i = 0; i <= words.Count - n; i++)
        {
            var ngram = string.Join(" ", words.Skip(i).Take(n));
            ngramscontinue...
 
```csharp
.Add(ngram);
        }
        return ngrams;
    }
 
    private int CountNgramOccurrences(string ngram)
    {
        int count = 0;
        for (int i = 0; i <= words.Count - ngram.Split(' ').Length; i++)
        {
            if (string.Join(" ", words.Skip(i).Take(ngram.Split(' ').Length)) == ngram)
            {
                count++;
            }
        }
        return count;
    }
 
    // This method uses the algorithm from https://stackoverflow.com/a/1271917
    private int CountSyllables(string word)
    {
        word = word.ToLower().Trim();
        if (word.Length <= 3)
        {
            return 1;
        }
 
        word = word.Replace("es", "");
        word = word.Replace("ed", "");
        word = word.Replace("e", "");
        var vowels = "aeiouy";
        int vowelCount = word.Count(c => vowels.Contains(c));
        int syllableCount = vowelCount +
            ((word.StartsWith("y") || vowels.Contains(word[0])) ? 0 : 1);
 
        return syllableCount;
    }
 
    private int CountLetters(string word)
    {
        return word.Count(char.IsLetter);
    }
}
class TextAnalyzer
{
    private readonly string text;
    private readonly List<string> sentences;
    private readonly List<string> words;
 
    public TextAnalyzer(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            throw new ArgumentException("Text cannot be null or empty.");
        }
 
        this.text = text;
        this.sentences = text.Split(new[] { '.', '?', '!' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(sentence => sentence.Trim())
            .ToList();
 
        this.words = text.Split(new[] { ' ', '\t', '\n' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(word => word.Trim(new[] { '.', ',', ';', ':', '(', ')', '[', ']', '{', '}', '<', '>', '\'', '\"', '`', '“', '”', '‘', '’', '—', '-', '–', '—', '/', '\\', '|', '_', '*', '&', '^', '%', '$', '#', '@', '!', '?', '+', '=', '~', '§', '©', '®', '™', '·', '…', '°', '℃', '℉', '€', '£', '¥', '₹', '¢', '₽', '₱', '₪', '฿', '₫', '₩', '﷼' }))
            .ToList();
    }
public double AverageNumberOfWordsPerSentence()
    {
        int wordCount = words.Count;
        int sentenceCount = sentences.Count;
        return (double)wordCount / sentenceCount;
    }
 
    public double AverageSyllablesPerWord()
    {
        int syllableCount = words.Sum(word => CountSyllables(word));
        int wordCount = words.Count;
        return (double)syllableCount / wordCount;
    }
 
    public double AverageLettersPerWord()
    {
        int letterCount = words.Sum(word => CountLetters(word));
        int wordCount = words.Count;
        return (double)letterCount / wordCount;
    }
public double AverageSentencesPerParagraph()
    {
        int paragraphCount = text.Split(new[] { "\r\n\r\n", "\n\n" }, StringSplitOptions.RemoveEmptyEntries).Count();
        int sentenceCount = sentences.Count;
        return (double)sentenceCount / paragraphCount;
    }
 
    public double UseOfRepetitivePhrases()
    {
        int n = 3; // Use trigrams (3-word sequences) for example
        var ngrams = GetNGrams(n);
 
        var ngramCounts = new Dictionary<string, int>();
        foreach (var ngram in ngrams)
        {
            if (ngramCounts.ContainsKey(ngram))
            {
                ngramCounts[ngram]++;
            }
            else
            {
                ngramCounts.Add(ngram, 1);
            }
        }
 
        int repetitivePhraseCount = ngramCounts.Values.Count(count => count > 1);
        return (double)repetitivePhraseCount / sentences.Count;
    }
public double UseOfMisnomers()
    {
        var misnomerPairs = new Dictionary<string, string>()
        {
            { "hot ice", "water" },
            { "sunrise at sunset", "horizon" }
            // Add more misnomer pairs as needed
        };
 
        int misnomerCount = sentences.Count(sentence =>
            misnomerPairs.Any(pair =>
                sentence.ToLower().Contains(pair.Key) && !sentence.ToLower().Contains(pair.Value)));
 
        return (double)misnomerCount / sentences.Count;
    }
 
    public double CommonalityOfWordGroupings(int groupingSize)
    {
        var ngrams = GetNGrams(groupingSize);
        int commonGroupingCount = ngrams.Sum(ngram => CountNgramOccurrences(ngram));
        return (double)commonGroupingCount / sentences.Count;
    }
public double FrequencyOfAlternativeSpelling()
    {
        var alternativeSpellings = new Dictionary<string, string>()
        {
            { "color", "colour" },
            { "analyze", "analyse" }
            // Add more alternative spellings as needed
        };
 
        int alternativeSpellingCount = words.Count(word =>
            alternativeSpellings.Any(pair =>
                string.Equals(word, pair.Key, StringComparison.OrdinalIgnoreCase) ||
                string.Equals(word, pair.Value, StringComparison.OrdinalIgnoreCase)));
 
        return (double)alternativeSpellingCount / sentences.Count;
    }
 
    private List<string> GetNGrams(int n)
    {
        var ngrams = new List<string>();
        for (int i = 0; i <= words.Count - n; i++)
        {
            var ngram = string.Join(" ", words.Skip(i).Take(n));
            ngramscontinue...
 
```csharp
.Add(ngram);
        }
        return ngrams;
    }
 
    private int CountNgramOccurrences(string ngram)
    {
        int count = 0;
        for (int i = 0; i <= words.Count - ngram.Split(' ').Length; i++)
        {
            if (string.Join(" ", words.Skip(i).Take(ngram.Split(' ').Length)) == ngram)
            {
                count++;
            }
        }
        return count;
    }
 
    // This method uses the algorithm from https://stackoverflow.com/a/1271917
    private int CountSyllables(string word)
    {
        word = word.ToLower().Trim();
        if (word.Length <= 3)
        {
            return 1;
        }
 
        word = word.Replace("es", "");
        word = word.Replace("ed", "");
        word = word.Replace("e", "");
        var vowels = "aeiouy";
        int vowelCount = word.Count(c => vowels.Contains(c));
        int syllableCount = vowelCount +
            ((word.StartsWith("y") || vowels.Contains(word[0])) ? 0 : 1);
 
        return syllableCount;
    }
 
    private int CountLetters(string word)
    {
        return word.Count(char.IsLetter);
    }
}
// Program.cs
 
using System;
 
class Program
{
    static void Main()
    {
        string text = "This is an example text. It contains multiple sentences. " +
            "Each sentence demonstrates the use of different punctuation marks! " +
            "In addition, it contains some misnomers like 'hot ice' and 'sunrise at sunset'. " +
            "Furthermore, it uses alternative spellings like 'color' and 'analyse'. " +
            "Lastly, it contains some repetitive phrases like 'repetitive phrases like'.";
 
        var analyzer = new TextAnalyzer(text);
 
        Console.WriteLine($"Average number of words per sentence: {analyzer.AverageNumberOfWordsPerSentence():0.00}");
        Console.WriteLine($"Average number of syllables per word: {analyzer.AverageSyllablesPerWord():0.00}");
        Console.WriteLine($"Average number of letters per word: {analyzer.AverageLettersPerWord():0.00}");
        Console.WriteLine($"Average number of sentences per paragraph: {analyzer.AverageSentencesPerParagraph():0.00}");
        Console.WriteLine($"Use of repetitive phrases: {analyzer.UseOfRepetitivePhrases():0.00}");
        Console.WriteLine($"Use of misnomers: {analyzer.UseOfMisnomers():0.00}");
        Console.WriteLine($"Commonality of word groupings of size 3: {analyzer.CommonalityOfWordGroupings(3):0.00}");
        Console.WriteLine($"Frequency of alternative spelling: {analyzer.FrequencyOfAlternativeSpelling():0.00}");
    }
}
